'use strict';

let getDataButton = document.querySelector('#spaceXdata');
let tableData = document.querySelector('#tableData');
let dataSpaceX;

getDataButton.addEventListener('click', getData);

async function getData() {
    let response = await fetch("https://api.spacexdata.com/v4/ships");
    let data = await response.json();
    dataSpaceX = data;
    console.log(dataSpaceX); //control console log
    replaceData();
    printData();
}

function replaceData() {
    for (let i = 0; i < dataSpaceX.length; i++) {
        if (dataSpaceX[i].active == true) {
            dataSpaceX[i].active = 'YES';
        }
        if (dataSpaceX[i].active == false) {
            dataSpaceX[i].active = 'NO';
        }
        if (dataSpaceX[i].active == null) {
            dataSpaceX[i].active = 'N/A';
        }
        if (dataSpaceX[i].home_port == null) {
            dataSpaceX[i].home_port = 'N/A';
        }
        if (dataSpaceX[i].legacy_id == null) {
            dataSpaceX[i].legacy_id = 'N/A';
        }
        if (dataSpaceX[i].mass_kg == null) {
            dataSpaceX[i].mass_kg = 'N/A';
        }
        if (dataSpaceX[i].model == null) {
            dataSpaceX[i].model = 'N/A';
        }
        if (dataSpaceX[i].name == null) {
            dataSpaceX[i].name = 'N/A';
        }
        if (dataSpaceX[i].type == null) {
            dataSpaceX[i].type = 'N/A';
        }
        if (dataSpaceX[i].year_built == null) {
            dataSpaceX[i].year_built = 'N/A';
        }
    }
};

function printData() {

    tableData.innerHTML = '';
    for (let i = 0; i < dataSpaceX.length; i++) {
        tableData.innerHTML += `
        <tr>
        <td>${dataSpaceX[i].active}</td>
        <td>${dataSpaceX[i].home_port}</td>
        <td>${dataSpaceX[i].launches.length}</td>
        <td>${dataSpaceX[i].legacy_id}</td>
        <td>${dataSpaceX[i].mass_kg}</td>
        <td>${dataSpaceX[i].model}</td>
        <td>${dataSpaceX[i].name}</td>
        <td>${dataSpaceX[i].type}</td>
        <td>${dataSpaceX[i].year_built}</td>
        </tr>
        `
    }
};